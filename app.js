/**
 * Created by rknell on 24/02/2014.
 */

var express = require("express");
var http = require("http");
var phantom = require("phantom");
var fs = require("fs");

app = express();
app.set('port', process.env.PORT || 3012);

var counter = 21000;

app.get("/status", function(req, res){
    res.send("server up");
});

app.get("/b64/:url", function(req, res){
    console.log("app get called b64");
    var decoded = new Buffer(req.params.url, "base64");
    renderpdf(decoded.toString(), res);
});

app.get("/favicon.ico", function(req,res){
    res.status(404).send();
})

app.get("/:url", function(req, res){
    console.log("app get called non b64");
    renderpdf(req.params.url, res);
});

function renderpdf(url, res){
    console.log(url);
    counter++
    console.log(counter);
    phantom.create(null, { port: counter}, function(ph){
        ph.createPage(function(page) {
            page.set("paperSize", { format: "A4", orientation: 'portrait', margin: '1cm' });
            page.open("http://" + url, function(status) {
                var fileName = makeid(5) + ".pdf";
                sendPDF(page, fileName, ph, res);
            })
        })
    });
}

function sendPDF(page, fileName, ph, res){
    page.render(fileName, function(){
        console.log("page rendered, sending file");
        res.status(200).sendfile(fileName,fileName, function(){
            ph.exit();
            fs.unlink(fileName, function(err){
                console.log('deleted file');
            })
        });
    })
}

console.log("Launching App!");

http.createServer(app).listen(app.get("port"), function() {
    console.log("Express server listening on port " + app.get("port"));
});

function makeid(strLength)
{
    strLength = (typeof optionalArg === "undefined") ? 5 : strLength;
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < strLength; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}